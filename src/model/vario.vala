/* Copyright 2021 Katharina Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Model {
    public class Vario {
        public uint x;
        public uint y;
    }
}

