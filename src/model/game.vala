/* Copyright 2021 Katharina Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Model {
    public class Game {
        public Vario vario;

        public Game () {
            this.vario = new Vario ();
        }
    }
}

