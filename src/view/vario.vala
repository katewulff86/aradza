/* Copyright 2021 Katharina Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace View {
    public class Vario : Drawable {
        private Model.Vario model;
        private unowned SDL.Video.Renderer renderer;
        private SDL.Video.Surface idle_animation;
        private SDL.Video.Texture idle_texture;

        public Vario (Model.Vario model, SDL.Video.Renderer renderer) {
            this.model = model;
            this.renderer = renderer;
            idle_animation = SDLImage.load (Config.VARIO_SPRITES_DIRECTORY + "/idle.png");
            if (idle_animation == null) {
                stdout.printf ("could not load image\n");
            }
            idle_texture = SDL.Video.Texture.create_from_surface (renderer,
                                                                  idle_animation);
        }

        public void draw (double time) {
            SDL.Video.Rect src_rect = {0, 0, 32, 32};
            SDL.Video.Rect dst_rect = {0, 100, 32, 32};
            renderer.copy (idle_texture, src_rect, dst_rect);
        }
    }
}

