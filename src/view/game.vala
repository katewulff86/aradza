/* Copyright 2021 Katharina Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace View {
    public class Game : Drawable {
        private unowned SDL.Video.Renderer renderer;
        private Model.Game game_model;
        private Vario vario;

        public Game (Model.Game game, SDL.Video.Renderer renderer) {
            this.renderer = renderer;
            this.game_model = game;
            this.vario = new Vario (game.vario, renderer);
            clear ();
        }

        public void draw (double time) {
            clear ();
            draw_background ();
            vario.draw (time);
            renderer.present ();
        }

        private void clear () {
            renderer.set_draw_color (0, 0, 0, 255);
            renderer.clear ();
        }

        private void draw_background () {
            renderer.set_draw_color (135, 209, 235, 255);
            SDL.Video.Rect rect = {0, 0, 256, 144};
            renderer.fill_rect (rect);
        }
    }
}

