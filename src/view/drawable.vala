/* Copyright 2021 Katharina Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace View {
    public interface Drawable {
        public abstract void draw (double time);
    }
}

