/* Copyright 2021 Katharina Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

[CCode (cheader_filename = "config.h", cprefix = "", lower_case_cprefix = "")]
namespace Config {
    public const string PACKAGE;
    public const string GETTEXT_PACKAGE;
    public const string LOCALEDIR;
    public const string VERSION;
    public const string DATA_DIRECTORY;
    public const string SPRITES_DIRECTORY;
    public const string VARIO_SPRITES_DIRECTORY;
    public const string SOUND_DIRECTORY;
}
