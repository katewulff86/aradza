/* Copyright 2021 Katharina Wulff <katty.wulff@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

int main (string[] args) {
    (void) args;
    bool should_quit = false;

    SDL.init ();
    SDLImage.init (SDLImage.InitFlags.PNG);

    var window = new SDL.Video.Window ("Vario Land",
                                       (int) SDL.Video.Window.POS_UNDEFINED, (int) SDL.Video.Window.POS_UNDEFINED,
                                       640, 480,
                                       SDL.Video.WindowFlags.RESIZABLE |
                                       SDL.Video.WindowFlags.ALLOW_HIGHDPI);
    var renderer = SDL.Video.Renderer.create (window, -1, 0);
    renderer.set_logical_size (256, 144);
    var game_model = new Model.Game ();
    var game_view = new View.Game (game_model, renderer);

    while (!should_quit) {
        var ticks = SDL.Timer.get_ticks ();
        double time = ticks / 1000.0;

        SDL.Event event;
        SDL.Event.poll (out event);
        switch (event.type) {
        case SDL.EventType.QUIT:
            should_quit = true;
            break;
        default:
            break;
        }

        game_view.draw (time);
    }

    SDLImage.quit ();
    SDL.quit ();

    return 0;
}

